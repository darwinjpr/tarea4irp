# tarea4_IRP

## Consideraciones iniciales:
1. Instalar python3 junto con las siguientes librerias necesarias, puede ser utilizando pip3
2. Abrir una terminal y navegar a la carpeta del proyecto con `cd tarea4IRP/`
 
### Librerias necesarias
 - idx2numpy
 - numpy
 - matplotlib.pyplot
 - sklearn
 - random     
 - math
 - joblib
 - tensorflow

Para instalar las librerias una opción es:
```
    $pip3 install libname
``` 
## Scikit-Learn & SVM
Se ha creado una interfaz de consola con el fin de dar facilidad de uso al usuario, donde se pueden ejecutar varias acciones referentes a máquinas de soporte vectorial.

En la carpeta del proyecto van incluidos un modelo para cada kernel, por lo que se puede cargar directamente sin tener que entrenar antes.

A continuación se muestra un ejemplo de uso como guía:

Desde la terminal descrita en consideraciones iniciales se escribe lo siguiente:
```
    $python3 mark4.py
```
lo que ejecuta el programa y muestra en la terminal lo siguiente:
```
    ¿Qué quiere hacer ahora?
    0:SALIR      1:ENTRENAR      2:GUARDAR   3:CARGAR
    4:VER SCORE  5:VER C_MATRIX  6:PREDECIR  7:PARÁMETROS
```
que es el menú principal de la interfáz.

Para entrenar un nuevo modelo, es necesario elegír antes los parámetros, por lo que se selecciona 'PARÁMETROS', de lo contrario se entrena un modelo con parámetros definidos en el código por defecto.

Entonces al escribir un 7 presionar ENTER, se muestra lo siguiente:
```
    Indique el numero del parámetro a cambiar:
    0:salir      1:gamma     2:C     3:max_iter      4:kernel    5:degree
```
que es el menú de opciones posibles a modificar para entrenar el modelo.

Para este ejemplo solo se va a modificar el kernel, ya que los valores predefinidos funcionan bien para el caso. Entonces, al escribir un 4 y presionar ENTER aparece en la ternimal lo siguiente:
```
    opciones para kernel:  ‘poly’, ‘rbf’, ‘sigmoid’
    Escriba cual kernel quiere utilizar: poly 
```
donde se ha escrito "poly" como la elección del kernel que hace referencia a polinomial. Al presionar ENTER nuevamente, se muestra el menu anterior, en el que se escribe 0 y se presiona ENTER para regresar al menu principal.
```
    ¿Qué quiere hacer ahora?
    0:SALIR      1:ENTRENAR      2:GUARDAR   3:CARGAR
    4:VER SCORE  5:VER C_MATRIX  6:PREDECIR  7:PARÁMETROS
```
Ahora bien, es posible entrenar el modelo por lo que se selecciona 'ENTRENAR', lo cual finaliza una vez que se muestra en la terminal que ya se ha entranado el modelo, como se muestra a continuación:
```
    entrenando...
    entrenamiento finalizado
```
Nuevamente el programa regresa al menú principal:
```
    ¿Qué quiere hacer ahora?
    0:SALIR      1:ENTRENAR      2:GUARDAR   3:CARGAR
    4:VER SCORE  5:VER C_MATRIX  6:PREDECIR  7:PARÁMETROS
```
Ahora, lo recomendado es guardar el programa, lo único a tomar en cuenta es que el programa guarda solamente 1 modelo por kernel, es decir, en total solamente hay 4 modelos.

Al seleccionar 'GUARDAR', el modelo es almacenado en la carpeta models/ con el nombre específico para el kernel seleccionado.
```
    guardando modelo para kernel poly...
    modelo guardado
```
Al seleccionar 'CARGAR' permite utilizar los modelos que ya han sido entrenados en sesiones previas para ver resultados, o predecir, lo que muestra la terminal es lo siguiente:
```
    Elija un modelo segun el kernel:
    1:poly, 2:rbf, 3:sigmoid
    1
```
Para el ejemplo se ha escrito '2' y se ha presionado ENTER, para seleccionar el kernel 'poly', por lo que muestra lo que aparece a continuación y regresa al menú principal.
```
    cargando...
    modelo cargado
```
Al seleccionar 'VER SCORE' calcula la presición con la que trabaja el modelo seleccionado, dando una calificación de 0 a 1. Continuando con el kernel 'poly' el resultado es:
```
    Calculando calificación...
    Esta es la calificación obtenida del entrenamiento:  0.9787
```
Al seleccionar 'VER C_MATRIX' calcula la matriz de confusión con los datos de test de MNIST, para el ejemplo el resultado de esto es:
```
    Se esta calculando la matriz de confusión...
    La matriz de confusión se muestra a continuación: 

    [[ 972    0    1    1    0    3    1    0    2    0]
     [   0 1126    2    1    1    0    3    0    2    0]
     [   8    0 1006    0    2    0    5    8    3    0]
     [   0    2    1  987    0    6    0    5    6    3]
     [   2    0    2    0  965    0    3    1    0    9]
     [   2    0    0   10    1  867    3    1    5    3]
     [   4    5    1    0    3    6  937    0    2    0]
     [   0   10    9    2    1    0    0 1000    0    6]
     [   5    0    1    3    4    4    1    4  950    2]
     [   3    6    1    5    9    3    1    1    3  977]]
    mostrando imagen de matriz de confusion

```
Como lo indica la terminal se muestra también una figura para apreciar mejor el resultado de forma visual, la cual también se puede guardar facilmente, para continuar con el programa debe cerrar esa figura.

Al seleccionar 'PREDECIR' se imprime en la terminal lo siguiente:
```
    Estamos haciendo una predicción para una imagen random
    imagen numero:  9163
    mostrando imagen a predecir
    Cierre la imagen para ver predicción.
```
Aquí se muestra la figura (en este caso un 3), seleccionada de los datos de test de forma aleatoria, para este caso es la que ocupa el indice [9163]. Nuevamente, para continuar con el programa debe cerrar esa figura.
```
    La predicción es:  [3]
```
Para finalizar el programa, solo se debe seleccionar 'SALIR', y la terminal vuelve a su estado inicial.

Ctrl+Shift+g para ver vista previa

## Keras & Deep Learning