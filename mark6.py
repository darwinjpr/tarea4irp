
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
import random
from skimage import io,color
print(tf.__version__)

mnist = tf.keras.datasets.mnist
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_prueba=x_test
#x_train = tf.keras.utils.normalize(x_train, axis=1)
#x_test = tf.keras.utils.normalize(x_test, axis=1)
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
p_epocas=3
p_batchsize=100
p_verbose=1
filename='epic_num_reader.model'

while 1:
    print('\n¿Qué quiere hacer ahora?')
    print('0:SALIR      1:ENTRENAR      2:GUARDAR   3:CARGAR')
    print('4:VER SCORE  5:VER C_MATRIX  6:PREDECIR  7:PARÁMETROS')
    sel = input()
    if int(sel)==0:#salir del bucle
        print('\nHasta luego!')
        break
    elif int(sel)==1: #entrenar
        print('\nentrenando...')
        model.fit(x_train, y_train, nb_epoch=p_epocas, batch_size=p_batchsize, verbose=p_verbose)
        print('entrenamiento finalizado')
    elif int(sel)==2: #guardar
        print('\nguardando modelo para keras...')
        model.save('models/'+filename)
        print('modelo guardado')
    elif int(sel)==3: #cargar
        print('\nCargando un modelo para keras...')
        model = tf.keras.models.load_model('models/'+filename)
        print('modelo cargado')
    elif int(sel)==4: #score
        print('Calculando calificación...')
        sc=model.evaluate(x_test,y_test, verbose=2)
        print('Esta es la calificación obtenida del entrenamiento: ', sc) 
    elif int(sel)==5: #matriz de confusion
        print('\nSe esta calculando la matriz de confusión...')
        predictions = model.predict(x_test)
        confusion=np.zeros((10,10))
        tamaño=10000
        i=0
        while i<tamaño:
            a=y_test[i]
            c=(np.argmax(predictions[i]))
            confusion[a,c]=int(confusion[a,c]+1)
            i=i+1
        #print(confusion)
        f=0
        while f<10:
            print(confusion[f,0],confusion[f,1],confusion[f,2],confusion[f,3],confusion[f,4],confusion[f,5],confusion[f,6],confusion[f,7],confusion[f,8],confusion[f,9])
            f=f+1
    elif int(sel)==6: #prediccion
        print('\nEstamos haciendo una predicción para una imagen random')
        index=random.randint(1,len(x_test))
        print('imagen numero: ', index)
        print('mostrando imagen a predecir')
        plt.imshow(np.reshape(x_test[index], (28,28)), cmap = plt.cm.binary)
        plt.show()

        P = model.predict(x_test)#recibe 
        print('La predicción es: ', np.argmax(P[index]))
        input('Presione ENTER para continuar.')
    elif int(sel)==7:#parámetros
        while 1:
            print('\nIndique el numero del parámetro a cambiar:')
            print('0:salir      1:epocas')
            sel = input()
            if(int(sel)==0):
                print('\ntodos los cambios actualizados!')
                break
            elif(int(sel)==1):
            	p_epocas=int(input('\nIndique el número de epocas: '))     
            elif(int(sel)==2):
                p_batchsize=int(input('\nIndique el tamaño de batch: '))
            elif(int(sel)==3):
                p_verbose=int(input('\nIndique el el parámetro verbose: '))
            else: 
                print('\nERROR:Instrucción no encontrada')
    elif int(sel)==8:#pruebas
        print('\nERROR:Instrucción no encontrada')
        image2=color.rgb2gray(io.imread("patrones2.png"))
        plt.imshow(image2,cmap=plt.cm.binary)
        plt.show()
        #para ver que es la imagen
        x_prueba[1]=image2
        #x_prueba = tf.keras.utils.normalize(x_prueba, axis=1)
        prueba = new_model.predict(x_prueba)
        print(np.argmax(prueba[1]))
    else: 
        print('\nERROR:Instrucción no encontrada')