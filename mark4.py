##mark4
##easy way to do an interface

import idx2numpy
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn import metrics
import random
import math
import joblib

#carga de datos
x_train = idx2numpy.convert_from_file('data/train-images.idx3-ubyte')
y_train = idx2numpy.convert_from_file('data/train-labels.idx1-ubyte')
x_test = idx2numpy.convert_from_file('data/t10k-images.idx3-ubyte')
y_test = idx2numpy.convert_from_file('data/t10k-labels.idx1-ubyte')
#reacomodo de datos
xx_train = np.reshape(x_train, (len(x_train),-1))
xx_test = np.reshape(x_test, (len(x_test),-1))

#valores por defecto
p_kernel='rbf'
p_gamma="scale"
p_C=100
p_max_iter=-1
p_degree=3

condicion_guardar=False
condicion_score=False

while 1:
    print('\n¿Qué quiere hacer ahora?')
    print('0:SALIR      1:ENTRENAR      2:GUARDAR   3:CARGAR')
    print('4:VER SCORE  5:VER C_MATRIX  6:PREDECIR  7:PARÁMETROS')
    sel = input()
    if int(sel)==0:#salir del bucle
        print('\nHasta luego!')
        break
    elif int(sel)==1: #entrenar
        print('\nentrenando...')
        model = svm.SVC(gamma=p_gamma,C=p_C,max_iter=p_max_iter,kernel=p_kernel,degree=p_degree)
        model.fit(xx_train,y_train)
        condicion_guardar=True
        print('entrenamiento finalizado')
    elif int(sel)==2: #guardar
        if condicion_guardar:
            if(p_kernel=='poly'):#poly
                filename = 'finalized_model_poly.sav'
                print('\nguardando modelo para kernel poly...')
            elif(p_kernel=='rbf'):#rbf
                filename = 'finalized_model_rbf.sav'
                print('\nguardando modelo para kernel rbf...')
            elif(p_kernel=='sigmoid'):#sigmoid
                filename = 'finalized_model_sigmoid.sav'
                print('\nguardando modelo para kernel sigmoid...')
            joblib.dump(model, 'models/'+filename)
            print('modelo guardado')
            print('\nguardando parámetros...')
            archivo = open('models/'+'data'+p_kernel+'.txt', 'w')
            archivo.write('Mostrando valores de entrenamiento: \n')
            if(type(p_gamma) == str):
                archivo.write('gamma    = '+p_gamma+'\n')
            else:
                archivo.write('gamma    = '+str(p_gamma)+'\n')
            archivo.write('C        = '+str(p_C)+'\n')
            archivo.write('max_iter = '+str(p_max_iter)+'\n')
            archivo.write('kernel   = '+p_kernel+'\n')
            archivo.write('degree   = '+str(p_degree)+'\n')
            archivo.close()
            print('parámetros guardados')
        else:
            print('Debe entrenar antes de poder guardar. \n')
            input('Presione ENTER para continuar.')
    elif int(sel)==3: #cargar
        print('\nElija un modelo segun el kernel:')
        print('1:poly, 2:rbf, 3:sigmoid')
        sel=input()
        if(int(sel)==1):#poly
            filename = 'finalized_model_poly.sav'
            p_kernel = 'poly'
        elif(int(sel)==2):#rbf
            filename = 'finalized_model_rbf.sav'
            p_kernel = 'rbf'
        elif(int(sel)==3):#sigmoid
            filename = 'finalized_model_sigmoid.sav'
            p_kernel = 'sigmoid'
        else:
            print('\nERROR: Valor no válido. Cargando modelo para rbf')
            filename = 'finalized_model_rbf.sav'
        print('\ncargando...')
        model = joblib.load('models/'+filename)
        condicion_guardar=False
        condicion_score=False
        print('modelo cargado')
    elif int(sel)==4: #score
        #exactitud, precision, sensibilidad, puntaje f1
        print('Calculando...\n')
        y_predict=model.predict(xx_test)
        exactitud=metrics.accuracy_score(y_test, y_predict)
        precision=metrics.precision_score(y_test, y_predict, average='macro')
        sensibilidad=metrics.recall_score(y_test, y_predict, average='macro')
        pF1=metrics.f1_score(y_test, y_predict, average='macro')
        #log_loss=metrics.hinge_loss(y_test, y_predict)
        condicion_score=True
        print('exactitud: ',exactitud)
        print('precision: ',precision)
        print('sensibilidad: ',sensibilidad)
        print('pF1: ',pF1)
        #print('loss: ',log_loss)
        input('Presione ENTER para continuar.')
    elif int(sel)==5: #matriz de confusion
        if condicion_score:
            print('\nSe esta calculando la matriz de confusión...')
            cm = metrics.confusion_matrix(y_true=y_test, y_pred=y_predict)
            print('La matriz de confusión se muestra a continuación: \n')
            print(cm)
            print('\nmostrando imagen de matriz de confusion')
            fig, ax = plt.subplots()
            im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Reds)
            ax.figure.colorbar(im, ax=ax)
            ax.set(xticks=np.arange(cm.shape[1]),yticks=np.arange(cm.shape[0]),
                        ylabel='True label',xlabel='Predicted label',
                        xlim=(-0.5, 9.5), ylim=(9.5,-0.5))
            thresh = cm.max() / 2.
            for i in range(cm.shape[0]):
                for j in range(cm.shape[1]):
                    ax.text(j, i, format(cm[i, j], 'd'),
                            ha="center", va="center",
                            color="white" if cm[i, j] > thresh else "black")
            fig.tight_layout()
            print('Cierre la imagen para continuar.')
            plt.show()
        else:
            print('Debe ver score antes de ver matriz de confusion. \n')
            input('Presione ENTER para continuar.')
    elif int(sel)==6: #prediccion
        print('\nEstamos haciendo una predicción para una imagen random')
        index=random.randint(1,len(xx_test))
        print('imagen numero: ', index)
        print('mostrando imagen a predecir')
        plt.imshow(np.reshape(xx_test[index], (28,28)), cmap = plt.cm.binary)
        print('Cierre la imagen para ver predicción.')
        plt.show()
        P = model.predict([xx_test[index]])#recibe 
        print('\nLa predicción es: ', P)
        input('Presione ENTER para continuar.')
    elif int(sel)==7:#parámetros
        while 1:
            print('\nIndique el numero del parámetro a cambiar:')
            print('0:salir     1:gamma     2:C     3:max_iter')
            print('4:kernel    5:degree    6:ver valores')
            sel = input()
            if(int(sel)==0):
                print('\ntodos los cambios actualizados!')
                break
            elif(int(sel)==1):
                gamma=input('\nIndique el valor de gamma: ')
            elif(int(sel)==2):
                C=input('\nIndique el valor de C: ')
            elif(int(sel)==3):
                max_iter=input('\nIndique valor para max_iter: ')
            elif(int(sel)==4):
                print('\nopciones para kernel:  ‘poly’, ‘rbf’, ‘sigmoid’')
                p_kernel=input('Escriba cual kernel quiere utilizar: ')
                if ((p_kernel == 'poly') or (p_kernel == 'rbf') or (p_kernel == 'sigmoid')):
                    pass
                else:
                    p_kernel = 'rbf'
                    print('\nERROR: el valor de kernel no es válido, inténtelo de nuevo.')
                    print('No se necesita escribir comillas al ingresar el nombre de kernel.')
                    print('Se guardó el kernel como `rbf´ que es el valor por defecto.')
            elif(int(sel)==5):
                degree=input('\nIndique el valor de degree: ')
            elif(int(sel)==6):#ver valores...
                print('\nMostrando valores actuales para entrenar:')
                print('gamma    = ',p_gamma)
                print('C        = ',p_C)
                print('max_iter = ',p_max_iter)
                print('kernel   = ',p_kernel)
                print('degree   = ',p_degree)
                input('\nPresione ENTER para continuar.')                                       
            else: 
                print('\nERROR:Instrucción no encontrada')
    elif int(sel)==8:#pruebas
        print('\nERROR:Instrucción no encontrada')
    else: 
        print('\nERROR:Instrucción no encontrada')