#ejemplo con sklearn simple (youtube)
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import svm
#import numpy as np

digits = datasets.load_digits()

clf = svm.SVC(gamma="scale")

x,y = digits.data[:-1], digits.target[:-1]

print(len(x),len(y))

clf.fit(x,y)

print(clf.score(x,y))

#P = clf.predict(digits.data[-1])
#print(model.score(x_test,y_test))
#print('Predicción: ', P )
plt.imshow(digits.images[-1], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()