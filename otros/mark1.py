import idx2numpy
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import random
import math
import joblib
#carga de datos
x_train = idx2numpy.convert_from_file('data/train-images.idx3-ubyte')
y_train = idx2numpy.convert_from_file('data/train-labels.idx1-ubyte')
x_test = idx2numpy.convert_from_file('data/t10k-images.idx3-ubyte')
y_test = idx2numpy.convert_from_file('data/t10k-labels.idx1-ubyte')
#reacomodo de datos
xx_train = np.reshape(x_train, (len(x_train),-1))
xx_test = np.reshape(x_test, (len(x_test),-1))

#probando prediccion
filename = 'finalized_model.sav'
print('cargando...')
model = joblib.load(filename)
print('modelo cargado')
print('Estamos haciendo una predicción para una imagen random')
index=random.randint(1,len(xx_test))
print(index)
print('mostrando imagen a predecir')
plt.imshow(np.reshape(xx_train[index], (28,28)), cmap = plt.cm.binary)
plt.show()
#print(xx_train[index])
print('shape: ', xx_test[index].shape)
P = model.predict([xx_train[index]])
print('La predicción es: ', P)

#cargar imagen y mostrar
#import matplotlib.pyplot as plt
from skimage import io,color


image=io.imread("patrones0.png") # imread lee las imagenes con los pixeles codificados como enteros 
# en el rango 0-255. Por eso la convertimos a flotante y en el rango 0-1
image2=color.rgb2gray(image)
print("- Dimensiones de la imagen:")
print(image2.shape)
print(np.reshape(image2, (784)).shape)
plt.imshow(image2)
plt.show()

print(np.reshape(image2, (784)).shape)
print(len(xx_test))



def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

y_predict = model.predict(xx_test)
print(xx_test[0],y_predict[0])
print('Vamos a imprimir la matriz de confusión:')
mac=plot_confusion_matrix(y_true=y_test,y_pred=y_predict, classes=['0','1','2','3','4','5','6','7','8','9'])