import idx2numpy
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
import random
import math
import joblib

#carga de datos
x_train = idx2numpy.convert_from_file('data/train-images.idx3-ubyte')
y_train = idx2numpy.convert_from_file('data/train-labels.idx1-ubyte')
x_test = idx2numpy.convert_from_file('data/t10k-images.idx3-ubyte')
y_test = idx2numpy.convert_from_file('data/t10k-labels.idx1-ubyte')
#reacomodo de datos
xx_train = np.reshape(x_train, (len(x_train),-1))
xx_test = np.reshape(x_test, (len(x_test),-1))


model = svm.SVC(gamma="scale", C=100)
filename = 'finalized_model.sav'
while 1:
	print('¿Qué quiere hacer ahora?')
	print('0:sel_parámetros 1:entrenar_set 		2:guardar_trabajo 3:cargar_trabajo 4:see_score 5:see_cmatrix') 
	print('6:ver_texto 		7:predecir_random 	8:salir')
	sel = input()
	if int(sel)==0:#parámetros
		print('forma:')
	elif int(sel)==1: #entrenar
		print('entrenando...')
		model.fit(xx_train,y_train)
		print('entrenamiento finalizado')
	elif int(sel)==2: #guardar
		print('guardando...')
		joblib.dump(model, filename)
		print('modelo guardado')
	elif int(sel)==3: #cargar
		print('cargando...')
		model = joblib.load(filename)
		print('modelo cargado')
	elif int(sel)==4: #score
		print('Calculando calificación...')
		print('Esta es la calificación obtenida del entrenamiento: ', model.score(xx_test,y_test))
	elif int(sel)==5: #matriz de confusion
		print('Esta es la matriz de confución obtenida')
		print('matriz')
	elif int(sel)==6: #fake
		archivo = open("texto.txt", 'r') 
		for linea in archivo.readlines(): 
			print(linea) 
			sel = input ('¿Desea seguir?(0 o 1): ')
			if int(sel)==0:
				archivo.close()
				break
		archivo.close()
	elif int(sel)==7: #prediccion
		print('Estamos haciendo una predicción para una imagen random')
		index=random.randint(1,len(xx_test))
		print('mostrando imagen a predecir')
		plt.imshow(np.reshape(xx_train[index], (28,28)), cmap = plt.cm.binary)
		plt.show()
		P = model.predict([xx_test[index]])#recibe 
		print('La predicción es: ', P)
	elif int(sel)==8:#salir del bucle
		print('Hasta luego!')
		break
	else: 
		print('ERROR:Instrucción no encontrada')