print(__doc__)

import numpy as np
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

# import some data to play with
iris = datasets.load_iris()
X = iris.data
y = iris.target
class_names = iris.target_names
class_primes = ['perro' , 'gato' ,  'trump']
print(class_names)
print(class_primes)

y=list(unique_labels(class_names, class_primes))

print(y)