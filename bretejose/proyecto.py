import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=FutureWarning)
    import tensorflow as tf
    from tensorflow import keras
    from tensorflow.keras.preprocessing.text import Tokenizer
    import matplotlib.pyplot as plt
    import numpy as np

print(tf.__version__)

mnist = tf.keras.datasets.mnist
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_prueba=x_test
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=3)
import matplotlib.pyplot as plt
#para guardar modelo
model.save('epic_num_reader.model')

new_model = tf.keras.models.load_model('epic_num_reader.model')
predictions = new_model.predict(x_test)
import numpy as np

#para matriz de confusion
import numpy
confusion=numpy.zeros((10,10))
tamaño=10000
i=10
while i<tamaño:
    a=y_test[i]
    c=(np.argmax(predictions[i]))
    confusion[a,c]=int(confusion[a,c]+1)
    i=i+1
#print(confusion)
f=0
while f<10:
    print(confusion[f,0],confusion[f,1],confusion[f,2],confusion[f,3],confusion[f,4],confusion[f,5],confusion[f,6],confusion[f,7],confusion[f,8],confusion[f,9])
    f=f+1



##para la imagen
from skimage import io
from skimage import io,color
image=io.imread("patrones2.png")
image2=color.rgb2gray(image)
plt.imshow(image2,cmap=plt.cm.binary)
plt.show()
#para ver que es la imagen
x_prueba[1]=image2
x_prueba = tf.keras.utils.normalize(x_prueba, axis=1)
prueba = new_model.predict(x_prueba)
print(np.argmax(prueba[1]))
#evalua modelo
results = model.evaluate(x_test, y_test, batch_size=128)
print('test loss, test acc:', results)
